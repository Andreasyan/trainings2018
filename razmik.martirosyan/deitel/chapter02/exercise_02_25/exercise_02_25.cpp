#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;
    
    if (0 == number2) {
        std::cout << "Error 1: Division by zero" << std::endl;
	return 1;
    }
    if (number1 % number2 == 0) {
        std::cout << "the first number is a multiple of the second" << std::endl;
	return 0;
    }
    std::cout << "the first number is not a multiple of the second" << std::endl;
    return 0;
}
