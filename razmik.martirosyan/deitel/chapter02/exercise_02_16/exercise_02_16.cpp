#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter the number 1: ";
    std::cin >> number1;
    std::cout << "Enter the number 2: ";
    std::cin >> number2;

    std::cout << "Sum = " << (number1 + number2) << std::endl;
    std::cout << "Difference = " << (number1 - number2) << std::endl;
    std::cout << "Product = " << (number1 * number2) << std::endl;

    if (0 == number2) {
        std::cout << "Error 1: The number can not be divided to zero" << std::endl;
	return 1;
    }
    std::cout << "Quotient = " << (number1 / number2) << std::endl;
    return 0;
}
