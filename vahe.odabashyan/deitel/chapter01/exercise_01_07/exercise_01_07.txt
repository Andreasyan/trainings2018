Why is so much attention today focused on object-oriented programming in general and C++ in particular?

Today, the demand for large-scale programs grows continuously. OOP enables not only software reuse, but also it makes easy the modification, maintenance and debugging of new software. The OOP reflects real-world objects, making programs more realistic. C++ has all the necessary tools to implement OOP approach. The use of classes and functions included in the Standard Library of C++ helps create programs that are performance efficient, portable and reusable.
