a-Fatal errors do not allow the program to run. They stop the program.
b-No fatal mistakes allow the program to pass, but the program may be wrong.
c-This is one of the main reasons, sometimes more likely to be a fatal error than non-lethal.
