1.6  Fill in the blanks with the following suggestions:

a) What logical computer device receives information from outside,
so that the computer uses it?
input device

b)The process of instructing the computer to solve specific problems
is called
programming

c)What type of computer language does it use for machine instructions?
language English-speaking abbreviations? 
assembler

d)Which logical device of the computer sends the already processed
computer information to various devices so that this information can
be used outside the computer?
Output device

e)What logical computer device stores information?
Memory device

f)What logic device does the computation do?
ALU

g)What logic device of the computer makes logical decisions?
ALU

h)The level of the computer language that is most suitable for the programmer for
quick and easy to write programs, is
high level

i)The only language that the computer understands directly
is an
machine language

j)What logical device of the computer coordinates the activities of all
other logical devices?
processor
