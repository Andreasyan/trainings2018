#include "Employee.hpp"
#include <iostream>

int
main()
{
    Employee employee1("Eduard", "Topchyan", 680);
    Employee employee2("Ruzanna","Sirunyan", 965);

    std::cout << "The yearly salary of "
        << employee1.getFirstName() << " "
        << employee1.getLastName() << " was "
        << employee1.getYearlySalary() << "$" << std::endl;
    std::cout << "The yearly salary of "
        << employee2.getFirstName() << " "
        << employee2.getLastName() << " was "
        << employee2.getYearlySalary() << "$" << std::endl;

    std::cout << "\nGOOD NEWS: The monthly salaries of each employee have been raised by 10 percent. Congratulations!\n" << std::endl;
    employee1.setMonthlySalary(employee1.getMonthlySalary() * 1.1);
    employee2.setMonthlySalary(employee2.getMonthlySalary() * 1.1);

    std::cout << "The yearly salary of "
        << employee1.getFirstName() << " "
        << employee1.getLastName() << " is "
        << employee1.getYearlySalary() << "$ now" << std::endl;
    std::cout << "The yearly salary of "
        << employee2.getFirstName() << " "
        << employee2.getLastName() << " is "
        << employee2.getYearlySalary() << "$ now" << std::endl;
    return 0;
}

