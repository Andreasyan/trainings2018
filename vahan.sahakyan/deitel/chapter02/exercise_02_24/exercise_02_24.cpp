#include <iostream>

int
main()
{
    int number;
    std::cout << "Insert a number: ";
    std::cin >> number;
    
    int remainder = number % 2;
    if (0 == remainder) {
        std::cout << "This number is even." << std::endl;
        return 0;
    }
    std::cout << "This number is odd." << std::endl;
    return 0;
}

