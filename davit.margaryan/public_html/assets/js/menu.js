(function() {
    var menu = document.getElementById('navBar');
    var menuWidth = menu.offsetWidth
    if (menuWidth < 900) {return false;}
    var maneuPages = document.getElementsByClassName('menu-page');
    for (var i = 0; i < maneuPages.length; ++i) {
        var factor = 23.5 * maneuPages.length;
        maneuPages[i].style.padding = "16px " + (menuWidth /  factor - 0.5) + "%";
        maneuPages[i].style.fontSize = menuWidth /  factor / 10 + "em";
    }
})()
